package gamf.mobildemo.data;

import java.util.List;

public class GifEntry {
    public String id;
    public String title;
    public String previewUrl;
    public String gifUrl;
    public long gifSize;
    public List<String> tags;
}

package gamf.mobildemo.ui.details;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import gamf.mobildemo.R;

public class MediaDetailsFragment extends Fragment {
    private static final String ARG_ID_PARAM = "id_param";
    private String idParam;

    private TextView detailsTitleTextView;
    private ImageView detailsMediaImageView;
    private TextView detailsTagsTextView;

    public MediaDetailsFragment() {
    }

    public static MediaDetailsFragment newInstance(String idParam) {
        MediaDetailsFragment fragment = new MediaDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ID_PARAM, idParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idParam = getArguments().getString(ARG_ID_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_media_details, container, false);

        detailsTitleTextView = rootView.findViewById(R.id.detailsTitleTextView);
        detailsMediaImageView = rootView.findViewById(R.id.detailsMediaImageView);
        detailsTagsTextView = rootView.findViewById(R.id.detailsTagsTextView);

        return rootView;
    }
}

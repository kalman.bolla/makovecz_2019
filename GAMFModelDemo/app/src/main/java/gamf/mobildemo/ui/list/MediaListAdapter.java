package gamf.mobildemo.ui.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gamf.mobildemo.R;
import gamf.mobildemo.data.GifEntry;

public class MediaListAdapter extends
        RecyclerView.Adapter<MediaListAdapter.MediaItemHolder> {

    private List<GifEntry> mediaList;
    private Context context;

    public MediaListAdapter(List<GifEntry> mediaList, Context context) {
        this.mediaList = mediaList;
        this.context = context;
    }

    @NonNull
    @Override
    public MediaItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.media_list_item, parent, false);

        return new MediaItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MediaItemHolder holder, int position) {
        GifEntry entry = mediaList.get(position);
        holder.titleTextView.setText(entry.title);

        // TODO: replace with Glide
        holder.mediaImageView.setImageResource(R.drawable.gamf_logo);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < entry.tags.size(); i++) {
            sb.append(entry.tags.get(i));
            if(i < entry.tags.size()-1) {
                sb.append(", ");
            }
        }

        holder.tagsTextView.setText(sb.toString());
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    public class MediaItemHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public ImageView mediaImageView;
        public TextView tagsTextView;

        public MediaItemHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            mediaImageView = itemView.findViewById(R.id.mediaImageView);
            tagsTextView =  itemView.findViewById(R.id.tagsTextView);
        }
    }

}

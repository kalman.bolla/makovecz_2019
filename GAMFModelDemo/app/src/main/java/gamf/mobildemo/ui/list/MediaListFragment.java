package gamf.mobildemo.ui.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gamf.mobildemo.R;
import gamf.mobildemo.data.GifEntry;
import gamf.mobildemo.services.MockGifService;

public class MediaListFragment extends Fragment {
    private OnMediaListFragmentInteractionListener mListener;

    private RecyclerView recyclerView;
    private Button addMediaButton;
    private List<GifEntry> mediaList;
    private MediaListAdapter adapter;

    public MediaListFragment() {
        // Required empty public constructor
    }

    public static MediaListFragment newInstance() {
        MediaListFragment fragment = new MediaListFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mediaList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_media_list, container, false);

        // TODO: replace
        final List<GifEntry> mediaList = MockGifService.getData();

        recyclerView = rootView.findViewById(R.id.mediaRecylerView);
        addMediaButton = rootView.findViewById(R.id.addMediaButton);

        adapter = new MediaListAdapter(mediaList, getActivity());
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getActivity()
                        .getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // item touch
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(),
                recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                GifEntry entry = mediaList.get(position);
                Toast.makeText(getActivity().getApplicationContext(),
                        entry.title + " is selected!", Toast.LENGTH_SHORT).show();

                mListener.onFragmentInteraction(entry.id);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        recyclerView.setAdapter(adapter);

        //addMediaButton.setOnClickListener(addMediaEvent);
        addMediaButton.setOnClickListener(titleSearchEvent);

        return rootView;
    }

    View.OnClickListener titleSearchEvent = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // TODO: add search
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMediaListFragmentInteractionListener) {
            mListener = (OnMediaListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMediaListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMediaListFragmentInteractionListener {
        void onFragmentInteraction(String gifId);
    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }
}

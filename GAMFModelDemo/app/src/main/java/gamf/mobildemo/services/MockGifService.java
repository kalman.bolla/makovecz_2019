package gamf.mobildemo.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import gamf.mobildemo.data.GifEntry;

public class MockGifService {
    public static List<GifEntry> getData() {
        List<GifEntry> gifEntryList = new ArrayList<>();

        GifEntry gif1 = new GifEntry();
        gif1.id = "1";
        gif1.title = "GAMF";
        gif1.previewUrl = "https://www.uni-neumann.hu/images/kar_ikons/gamf_nagy.jpg";
        gif1.gifUrl = "https://www.uni-neumann.hu/images/kar_ikons/gamf_nagy.jpg";
        gif1.gifSize = 12345;
        gif1.tags = Arrays.asList("nje", "gamf", "info");

        gifEntryList.add(gif1);


        GifEntry gif2 = new GifEntry();
        gif2.id = "2";
        gif2.title = "GAMF";
        gif2.previewUrl = "https://www.uni-neumann.hu/images/kar_ikons/gamf_nagy.jpg";
        gif2.gifUrl = "https://www.uni-neumann.hu/images/kar_ikons/gamf_nagy.jpg";
        gif2.gifSize = 12345;
        gif2.tags = Arrays.asList("nje", "gamf", "info");

        gifEntryList.add(gif2);

        return gifEntryList;
    }
}

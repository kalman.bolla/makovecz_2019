package gamf.mobildemo;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import gamf.mobildemo.ui.details.MediaDetailsFragment;
import gamf.mobildemo.ui.list.MediaListFragment;

public class MainActivity extends AppCompatActivity implements MediaListFragment.OnMediaListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MediaListFragment fragment = MediaListFragment.newInstance();
        loadFragment(fragment, "list");
    }

    private void loadFragment(Fragment fragment, String name) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(name)
                .commit();
    }

    @Override
    public void onFragmentInteraction(String gifId) {
        MediaDetailsFragment fragment = MediaDetailsFragment.newInstance(gifId);
        loadFragment(fragment, "details");
    }
}
